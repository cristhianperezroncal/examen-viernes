package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.IngredientXDishDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IngredientXDish {
    private int id;
    private Ingredient ingredient;
    private int quantity;

    public IngredientXDish(IngredientXDishDTO ingredientXDishDTO) {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(ingredientXDishDTO.getIdIngredient());
        this.ingredient = ingredient;
        this.quantity = ingredientXDishDTO.getQuantity();
    }
}
