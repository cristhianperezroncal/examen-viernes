package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.PurchaseOrderDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.annotation.Order;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrder {
    private int id;
    private double totalPrice;
    private List<OrderDetail> orderDetails;

    public PurchaseOrder(List<OrderDetail> orderDetails) {
        double total = 0;
        for (int i = 0; i < orderDetails.size(); i++) {
            total += orderDetails.get(i).getDish().getDishPrice();
        }
        this.totalPrice = total;
    }
}
