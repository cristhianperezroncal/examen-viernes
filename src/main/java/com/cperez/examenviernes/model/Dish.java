package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.DishDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dish {
    private int id;
    private DishType dishType;
    private String name;
    private String description;
    private double dishPrice;
    private List<IngredientXDish> ingredients;

    public Dish(DishDTO dishDTO) {
        DishType dishTypeOfDTO = new DishType();
        dishTypeOfDTO.setId(dishDTO.getIdDishType());
        this.dishType = dishTypeOfDTO;
        this.name = dishDTO.getName();
        this.description = dishDTO.getDescription();
    }

}
