package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.DishTypeDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DishType {
    private int id;
    private String name;
    public DishType(DishTypeDTO dishTypeDTO) {
        this.name = dishTypeDTO.getName();
    }
}
