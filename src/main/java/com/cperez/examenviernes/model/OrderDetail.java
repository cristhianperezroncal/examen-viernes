package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.OrderDetailDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetail {
    private int id;
    private Dish dish;
    public OrderDetail(Dish dish) {
        this.dish = dish;
    }
}
