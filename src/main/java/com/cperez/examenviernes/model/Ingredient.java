package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.IngredientDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class Ingredient {
    private int id;
    private String name;
    private double ingredientPrice;

    public Ingredient(IngredientDTO ingredientDTO) {
        this.name = ingredientDTO.getName();
        this.ingredientPrice = ingredientDTO.getIngredientPrice();
    }


}
