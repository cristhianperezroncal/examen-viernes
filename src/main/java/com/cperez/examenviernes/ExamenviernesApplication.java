package com.cperez.examenviernes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenviernesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenviernesApplication.class, args);
	}

}
