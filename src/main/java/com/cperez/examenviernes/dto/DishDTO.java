package com.cperez.examenviernes.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DishDTO {
    private int idDishType;
    private String name;
    private String description;
    private ArrayList<IngredientXDishDTO> ingredientsDTO;

}
