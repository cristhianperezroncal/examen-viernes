package com.cperez.examenviernes.dto;
import com.cperez.examenviernes.model.Ingredient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IngredientXDishDTO {
    private int  idIngredient;
    private int quantity;
}
