package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.IngredientDTO;
import com.cperez.examenviernes.model.Ingredient;

import java.util.List;

public interface IngredientService {
    List<Ingredient> getIngredients();
    Ingredient getIngredientByid(int id);
    Ingredient createIngredient(IngredientDTO ingredientDTO);
}
