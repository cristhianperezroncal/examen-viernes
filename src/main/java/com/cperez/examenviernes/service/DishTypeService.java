package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.DishTypeDTO;
import com.cperez.examenviernes.model.DishType;

import java.util.List;

public interface DishTypeService {
    List<DishType> getDishTypes();
    DishType getDishTypeByid(int id);
    DishType createDishType(DishTypeDTO dishTypeDTO);
}
