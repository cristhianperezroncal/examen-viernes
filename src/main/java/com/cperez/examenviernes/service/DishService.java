package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.DishDTO;
import com.cperez.examenviernes.model.Dish;

import java.util.List;

public interface DishService {
    List<Dish> getDishes();
    Dish getDishByid(int id);
    Dish createDish(DishDTO dishDTO);
}
