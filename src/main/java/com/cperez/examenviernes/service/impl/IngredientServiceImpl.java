package com.cperez.examenviernes.service.impl;

import com.cperez.examenviernes.dto.IngredientDTO;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.repository.IngredientRepository;
import com.cperez.examenviernes.service.IngredientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IngredientServiceImpl implements IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    @Override
    public List<Ingredient> getIngredients() {
        log.info("IngredientServiceImpl");
        return ingredientRepository.getIngredients();
    }

    @Override
    public Ingredient getIngredientByid(int id) {
        return ingredientRepository.getIngredientByid(id);
    }

    @Override
    public Ingredient createIngredient(IngredientDTO ingredientDTO) {
        Ingredient ingredient = new Ingredient(ingredientDTO);
        return ingredientRepository.createIngredient(ingredient);
    }
}
