package com.cperez.examenviernes.service.impl;

import com.cperez.examenviernes.dto.DishDTO;
import com.cperez.examenviernes.dto.IngredientXDishDTO;
import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.model.IngredientXDish;
import com.cperez.examenviernes.repository.DishRepository;
import com.cperez.examenviernes.repository.DishTypeRepository;
import com.cperez.examenviernes.repository.IngredientRepository;
import com.cperez.examenviernes.repository.IngredientXDishRepository;
import com.cperez.examenviernes.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {
    @Autowired
    DishRepository dishRepository;
    @Autowired
    DishTypeRepository dishTypeRepository;
    @Autowired
    IngredientXDishRepository ingredientXDishRepository;

    @Autowired
    IngredientRepository ingredientRepository;

    @Override
    public List<Dish> getDishes() {
        List<Dish> dishes = dishRepository.getDishes();
        for (Dish dish : dishes) {
            dish.setDishType(dishTypeRepository.getDishTypeByid(dish.getDishType().getId()));
            List<IngredientXDish> ingredientXDishes = ingredientXDishRepository.getIngredientXDishesByDishId(dish.getId());
            for (IngredientXDish ingredientXDish:ingredientXDishes) {
                Ingredient ingredient = ingredientRepository.getIngredientByid(ingredientXDish.getIngredient().getId());
                ingredientXDish.setIngredient(ingredient);
            }
            dish.setIngredients(ingredientXDishes);
        }
        return dishes;
    }

    @Override
    public Dish getDishByid(int id) {
        Dish dish = dishRepository.getDishByid(id);
        dish.setDishType(dishTypeRepository.getDishTypeByid(dish.getDishType().getId()));
        List<IngredientXDish> ingredientXDishes = ingredientXDishRepository.getIngredientXDishesByDishId(id);
        for (IngredientXDish ingredientXDish:ingredientXDishes) {
            Ingredient ingredient = ingredientRepository.getIngredientByid(ingredientXDish.getIngredient().getId());
            ingredientXDish.setIngredient(ingredient);
        }
        dish.setIngredients(ingredientXDishes);
        return dish;
    }

    @Override
    public Dish createDish(DishDTO dishDTO) {
        List<IngredientXDish> ingredientXDishes = new ArrayList<>();

        Dish dish = new Dish(dishDTO);



        dish.setDishType(dishTypeRepository.getDishTypeByid(dish.getDishType().getId()));



        dish.setIngredients(ingredientXDishes);

        double price = 0;

        Ingredient ingredientP;

        for (int i = 0; i < dishDTO.getIngredientsDTO().size(); i++) {
            ingredientP = ingredientRepository.getIngredientByid(dishDTO.getIngredientsDTO().get(i).getIdIngredient());
            price += dishDTO.getIngredientsDTO().get(i).getQuantity()*ingredientP.getIngredientPrice();
        }



        dish.setDishPrice(price);

        System.out.println("Precio: "+ price);

        dish = dishRepository.createDish(dish);

        ArrayList<IngredientXDishDTO> ingredientXDishDTOS = dishDTO.getIngredientsDTO();
        for (IngredientXDishDTO ingredients : ingredientXDishDTOS) {
            IngredientXDish ingredientXDish = new IngredientXDish(ingredients);
            ingredientXDishRepository.createIngredientXDish(dish.getId(), ingredientXDish);
        }

        dish.setIngredients(ingredientXDishRepository.getIngredientXDishesByDishId(dish.getId()));

        List<IngredientXDish> ingredientXDishes1 = dish.getIngredients();
        for (IngredientXDish ingredientXDish : ingredientXDishes1) {
            ingredientXDish.setIngredient(ingredientRepository.getIngredientByid(ingredientXDish.getIngredient().getId()));
        }

        return dish;
    }
}
