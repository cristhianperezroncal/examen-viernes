package com.cperez.examenviernes.service.impl;

import com.cperez.examenviernes.dto.DishTypeDTO;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.repository.DishTypeRepository;
import com.cperez.examenviernes.service.DishTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishTypeServiceImpl implements DishTypeService {
    @Autowired
    private DishTypeRepository dishTypeRepository;
    @Override
    public List<DishType> getDishTypes() {
        return dishTypeRepository.getDishTypes();
    }

    @Override
    public DishType getDishTypeByid(int id) {
        return dishTypeRepository.getDishTypeByid(id);
    }

    @Override
    public DishType createDishType(DishTypeDTO dishTypeDTO) {
        DishType dishType = new DishType(dishTypeDTO);
        return dishTypeRepository.createDishType(dishType);
    }
}
