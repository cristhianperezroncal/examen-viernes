package com.cperez.examenviernes.service.impl;

import com.cperez.examenviernes.dto.OrderDetailDTO;
import com.cperez.examenviernes.dto.PurchaseOrderDTO;
import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.OrderDetail;
import com.cperez.examenviernes.model.PurchaseOrder;
import com.cperez.examenviernes.repository.DishRepository;
import com.cperez.examenviernes.repository.DishTypeRepository;
import com.cperez.examenviernes.repository.OrderDetailRepository;
import com.cperez.examenviernes.repository.PurchaseOrderRepository;
import com.cperez.examenviernes.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PurchaseOrderServiceImpl implements PurchaseOrderService {
    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @Autowired
    private DishRepository dishRepository;
    @Autowired
    private DishTypeRepository dishTypeRepository;

    @Override
    public List<PurchaseOrder> getPurchaseOrders() {
        List<PurchaseOrder> purchaseOrders = purchaseOrderRepository.getPurchaseOrders();
        for (PurchaseOrder purchaseOrder : purchaseOrders) {
            List<OrderDetail> orderDetails = orderDetailRepository.getOrderDetailByPurchaseOrderId(purchaseOrder.getId());
            for (OrderDetail orderDetail : orderDetails) {
                orderDetail.setDish(dishRepository.getDishByid(orderDetail.getDish().getId()));
                orderDetail.getDish().setDishType(dishTypeRepository.getDishTypeByid(orderDetail
                        .getDish().getDishType().getId()));
            }
            purchaseOrder.setOrderDetails(orderDetails);
        }
        return purchaseOrders;
    }

    @Override
    public PurchaseOrder getPurchaseOrderById(int id) {
        PurchaseOrder purchaseOrder = purchaseOrderRepository.getPurchaseOrderById(id);
        List<OrderDetail> orderDetails = orderDetailRepository.getOrderDetailByPurchaseOrderId(purchaseOrder.getId());
        for (OrderDetail orderDetail : orderDetails) {
            orderDetail.setDish(dishRepository.getDishByid(orderDetail.getDish().getId()));
            orderDetail.getDish().setDishType(dishTypeRepository.getDishTypeByid(orderDetail
                    .getDish().getDishType().getId()));
        }

        purchaseOrder.setOrderDetails(orderDetails);

        System.out.println("Price: "+purchaseOrder.getTotalPrice());
        return purchaseOrder;
    }

    @Override
    public PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {


            List<OrderDetail> orderDetails = new ArrayList<>();
            for (OrderDetailDTO orderDetailDTO: purchaseOrderDTO.getOrderDetailDTOS()) {
                System.out.println("Dish ID: "+orderDetailDTO.getIdDish());
                Dish dish = dishRepository.getDishByid(orderDetailDTO.getIdDish());
                //OrderDetail orderDetail = new OrderDetail(orderDetailDTO, dish);
                System.out.println("Dish: "+dish.getName());
                OrderDetail orderDetail = new OrderDetail(dish);
                System.out.println("Order Detail Dish: "+ orderDetail.getDish().getName());
                orderDetails.add(orderDetail);
            }
            PurchaseOrder purchaseOrder = new PurchaseOrder(orderDetails);
        //System.out.println("Price: "+purchaseOrder.getTotalPrice());
            purchaseOrder = purchaseOrderRepository.createPurchaseOrder(purchaseOrder);


            for (OrderDetail orderDetail: orderDetails) {
                orderDetailRepository.createOrderDetail(purchaseOrder.getId(), orderDetail);
                orderDetail.getDish().setDishType(dishTypeRepository.getDishTypeByid(orderDetail.getDish().getDishType().getId()));
            }
            purchaseOrder.setOrderDetails(orderDetails);
            return purchaseOrder;

    }
}
