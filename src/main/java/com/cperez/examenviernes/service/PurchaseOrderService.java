package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.PurchaseOrderDTO;
import com.cperez.examenviernes.model.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderService {

    List<PurchaseOrder> getPurchaseOrders();
    PurchaseOrder getPurchaseOrderById(int id);

    PurchaseOrder createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO);
}
