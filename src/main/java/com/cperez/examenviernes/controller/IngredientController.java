package com.cperez.examenviernes.controller;

import com.cperez.examenviernes.dto.IngredientDTO;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.service.IngredientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/ingredients")
@Slf4j
public class IngredientController {
    @Autowired
    private IngredientService ingredientService;

    @GetMapping("")
    public ResponseEntity<List<Ingredient>> getIngredients() {
        log.info("Controlador");
        try {
            List<Ingredient> ingredients = ingredientService.getIngredients();
            return ResponseEntity.status(HttpStatus.OK).body(ingredients);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<Ingredient> getIngredientByid(@PathVariable int id) {
        try {
            Ingredient ingredient = ingredientService.getIngredientByid(id);
            return ResponseEntity.status(HttpStatus.OK).body(ingredient);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping("")
    @Transactional
    public ResponseEntity<Ingredient> createIngredient(@RequestBody IngredientDTO ingredientDTO) {
        try {
            Ingredient ingredient = ingredientService.createIngredient(ingredientDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(ingredient);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }
}


