package com.cperez.examenviernes.controller;

import com.cperez.examenviernes.dto.DishDTO;
import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/dishes")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;

    @GetMapping("")
    public ResponseEntity<List<Dish>> getDishes() {
        try {
            List<Dish> dishes = dishService.getDishes();
            return ResponseEntity.status(HttpStatus.OK).body(dishes);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Dish> getDishByid(@PathVariable int id) {
        try {
            Dish dish = dishService.getDishByid(id);
            return ResponseEntity.status(HttpStatus.OK).body(dish);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping("")
    @Transactional
    public ResponseEntity<Dish> createDish(@RequestBody DishDTO dishDTO) {
        try {
            Dish dish = dishService.createDish(dishDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(dish);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }



}
