package com.cperez.examenviernes.controller;

import com.cperez.examenviernes.dto.DishTypeDTO;
import com.cperez.examenviernes.dto.IngredientDTO;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.service.DishTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/dishTypes")
@Slf4j
public class DishTypeController {
    @Autowired
    private DishTypeService dishTypeService;

    @GetMapping("")
    public ResponseEntity<List<DishType>> getDishTypes() {
        try {
            List<DishType> dishTypes = dishTypeService.getDishTypes();
            return ResponseEntity.status(HttpStatus.OK).body(dishTypes);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<DishType> getDishTypeByid(@PathVariable int id) {
        try {
            DishType dishType = dishTypeService.getDishTypeByid(id);
            return ResponseEntity.status(HttpStatus.OK).body(dishType);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping("")
    @Transactional
    public ResponseEntity<DishType> createDishType(@RequestBody DishTypeDTO dishTypeDTO) {
        try {
            DishType dishType = dishTypeService.createDishType(dishTypeDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(dishType);
        }
        catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }
}
