package com.cperez.examenviernes.repository.impl;

import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.model.IngredientXDish;
import com.cperez.examenviernes.repository.IngredientXDishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class IngredientXDishRepositoryImpl implements IngredientXDishRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    //Prueba tiene cuatro campos solo estamos seteando 3
    public IngredientXDish mapToIngredientXDish(ResultSet rs) throws SQLException {
        IngredientXDish ingredientXDish = new IngredientXDish();
        ingredientXDish.setId(rs.getInt("id"));

        Ingredient ingredient = new Ingredient();
        ingredient.setId(rs.getInt("idIngredient"));
        ingredientXDish.setIngredient(ingredient);

        ingredientXDish.setQuantity(rs.getInt("quantity"));
        return ingredientXDish;
    }

    @Override
    public List<IngredientXDish> getIngredientXDishesByDishId(int id) {
        String sql = "CALL GetIngredientsByDishId(?);";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToIngredientXDish(rs), id);
    }

    @Override
    public IngredientXDish createIngredientXDish(int idDish, IngredientXDish ingredientXDish) {
        String sql = "INSERT INTO IngredientXDish(idIngredient, idDish, quantity) VALUES (?, ?, ?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[] {"id"});
                    ps.setInt(1, ingredientXDish.getIngredient().getId());
                    ps.setInt(2, idDish);
                    ps.setInt(3, ingredientXDish.getQuantity());
                    return ps;
                }, keyHolder);
        ingredientXDish.setId(keyHolder.getKey().intValue());
        return ingredientXDish;
    }
}
