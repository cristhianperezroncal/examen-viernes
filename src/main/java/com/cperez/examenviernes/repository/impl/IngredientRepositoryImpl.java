package com.cperez.examenviernes.repository.impl;

import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.repository.IngredientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Slf4j
public class IngredientRepositoryImpl implements IngredientRepository {


    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcInsert simpleJdbcInsert;

    @Autowired
    public IngredientRepositoryImpl(JdbcTemplate jdbcTemplate, SimpleJdbcInsert simpleJdbcInsert) {
        this.jdbcTemplate = jdbcTemplate;
        this.simpleJdbcInsert = simpleJdbcInsert.withTableName("Ingredient").usingGeneratedKeyColumns("id");
    }



    public Ingredient mapToIngredient(ResultSet rs) throws SQLException {
        return new Ingredient(rs.getInt("id"), rs.getString("name"), rs.getDouble("ingredientPrice"));
    }

    @Override
    public List<Ingredient> getIngredients() {
        log.info("IngredientRepositorioImpl");
        String sql = "CALL GetAllIngredients();";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToIngredient(rs));
    }

    @Override
    public Ingredient getIngredientByid(int id) {
        String sql = "CALL GetIngredientById(?);";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToIngredient(rs), id);
    }

    @Override
    public Ingredient createIngredient(Ingredient ingredient) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", ingredient.getName());
        parameters.put("ingredientPrice", ingredient.getIngredientPrice());
        int id = simpleJdbcInsert.executeAndReturnKey(parameters).intValue();
        ingredient.setId(id);
        return ingredient;
    }
}
