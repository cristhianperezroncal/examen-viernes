package com.cperez.examenviernes.repository.impl;

import com.cperez.examenviernes.model.OrderDetail;
import com.cperez.examenviernes.model.PurchaseOrder;
import com.cperez.examenviernes.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PurchaseOrderRepositoryImpl implements PurchaseOrderRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public PurchaseOrder mapToPurchaseOrder(ResultSet rs) throws SQLException {
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setId(rs.getInt("id"));
        purchaseOrder.setTotalPrice(rs.getDouble("totalPrice"));
        return purchaseOrder;
    }

    @Override
    public List<PurchaseOrder> getPurchaseOrders() {
        String sql = "CALL GetAllPurchaseOrders();";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToPurchaseOrder(rs));
    }

    @Override
    public PurchaseOrder getPurchaseOrderById(int id) {
        String sql = "CALL GetPurchaseOrderById(?);";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToPurchaseOrder(rs), id);
    }

    @Override
    public PurchaseOrder createPurchaseOrder(PurchaseOrder purchaseOrder) {
        String sql = "INSERT INTO PurchaseOrder(totalPrice) VALUES(?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[] {"id"});
                    ps.setDouble(1, purchaseOrder.getTotalPrice());
                    return ps;
                }, keyHolder);
        purchaseOrder.setId(keyHolder.getKey().intValue());
        return purchaseOrder;

    }
}
