package com.cperez.examenviernes.repository.impl;

import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.repository.DishTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DishTypeRepositoryImpl implements DishTypeRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    public DishType mapToDishType(ResultSet rs) throws SQLException {
        return new DishType(rs.getInt("id"), rs.getString("name"));
    }
    @Override
    public List<DishType> getDishTypes() {
        String sql = "CALL GetAllDishTypes();";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToDishType(rs));
    }

    @Override
    public DishType getDishTypeByid(int id) {
        String sql = "CALL GetDishTypeById(?);";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToDishType(rs), id);
    }

    @Override
    public DishType createDishType(DishType dishType) {
        String sql = "INSERT INTO DishType(name) VALUES (?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" });
                    ps.setString(1, dishType.getName());
                    return ps;
                }, keyHolder);
        System.out.println("Key holder value: "+keyHolder.getKey());
        dishType.setId(keyHolder.getKey().intValue());
        return dishType;
    }
}
