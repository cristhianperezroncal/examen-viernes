package com.cperez.examenviernes.repository.impl;

import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.OrderDetail;
import com.cperez.examenviernes.repository.OrderDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class OrderDetailRepositoryImpl implements OrderDetailRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public OrderDetail mapToOrderDetail(ResultSet rs) throws SQLException {
        OrderDetail orderDetail = new OrderDetail();
        orderDetail.setId(rs.getInt("id"));
        Dish dish = new Dish();
        dish.setId(rs.getInt("idDish"));
        orderDetail.setDish(dish);
        return orderDetail;
    }

    @Override
    public List<OrderDetail> getOrderDetailByPurchaseOrderId(int id) {
        String sql = "CALL GetOrderDetailsByPurchaseOrderId(?);";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToOrderDetail(rs), id);
    }

    @Override
    public OrderDetail createOrderDetail(int idPurchaseOrder, OrderDetail orderDetail) {
        String sql = "INSERT INTO OrderDetail(idPurchaseOrder, idDish) VALUES(?, ?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[] {"id"});
                    ps.setInt(1, idPurchaseOrder);
                    ps.setInt(2, orderDetail.getDish().getId());
                    return ps;
                }, keyHolder);
        orderDetail.setId(keyHolder.getKey().intValue());
        return orderDetail;
    }
}
