package com.cperez.examenviernes.repository.impl;

import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DishRepositoryImpl implements DishRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public Dish mapToDish(ResultSet rs) throws SQLException {
        Dish dish = new Dish();
        dish.setId(rs.getInt("id"));

        DishType dishType = new DishType();
        dishType.setId(rs.getInt("idDishType"));

        dish.setDishType(dishType);

        dish.setName(rs.getString("name"));
        dish.setDescription(rs.getString("description"));
        dish.setDishPrice(rs.getDouble("dishPrice"));

        return dish;
    }

    @Override
    public List<Dish> getDishes() {
        String sql = "CALL GetAllDishes();";
        return jdbcTemplate.query(sql, (rs, rowNum) -> mapToDish(rs));
    }

    @Override
    public Dish getDishByid(int id) {
        String sql = "CALL GetDishById(?);";
        return jdbcTemplate.queryForObject(sql, (rs, rowNum) -> mapToDish(rs),id);
    }

    @Override
    public Dish createDish(Dish dish) {
        String sql = "INSERT INTO Dish(idDishType, name, description, dishPrice) VALUES (?, ?, ?, ?);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" });
                    ps.setInt(1, dish.getDishType().getId());
                    ps.setString(2, dish.getName());
                    ps.setString(3, dish.getDescription());
                    ps.setDouble(4, dish.getDishPrice());
                    return ps;
                }, keyHolder);
        dish.setId(keyHolder.getKey().intValue());
        return dish;
    }



}
