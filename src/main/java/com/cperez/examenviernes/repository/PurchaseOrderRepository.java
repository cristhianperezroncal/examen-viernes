package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderRepository  {
    List<PurchaseOrder> getPurchaseOrders();
    PurchaseOrder getPurchaseOrderById(int id);

    PurchaseOrder createPurchaseOrder(PurchaseOrder purchaseOrder);
}
