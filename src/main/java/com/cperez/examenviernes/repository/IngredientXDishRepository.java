package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.IngredientXDish;

import java.util.List;


public interface IngredientXDishRepository {
    List<IngredientXDish> getIngredientXDishesByDishId(int id);

    IngredientXDish createIngredientXDish(int idDish, IngredientXDish ingredientXDish);
}
