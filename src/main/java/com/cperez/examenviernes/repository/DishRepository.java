package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.Dish;

import java.util.List;

public interface DishRepository {
    List<Dish> getDishes();
    Dish getDishByid(int id);
    Dish createDish(Dish dish);
}
