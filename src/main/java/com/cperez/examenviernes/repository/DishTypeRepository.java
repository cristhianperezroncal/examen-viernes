package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.DishType;

import java.util.List;

public interface DishTypeRepository {
    List<DishType> getDishTypes();
    DishType getDishTypeByid(int id);
    DishType createDishType(DishType dishType);
}
