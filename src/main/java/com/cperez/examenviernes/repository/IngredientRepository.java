package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.Ingredient;

import java.util.List;


public interface IngredientRepository {
    List<Ingredient> getIngredients();
    Ingredient getIngredientByid(int id);
    Ingredient createIngredient(Ingredient ingredient);
}
