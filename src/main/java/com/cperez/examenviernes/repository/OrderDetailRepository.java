package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.OrderDetail;

import java.util.List;

public interface OrderDetailRepository {
    List<OrderDetail> getOrderDetailByPurchaseOrderId(int id);
    OrderDetail createOrderDetail(int idPurchaseOrder, OrderDetail orderDetail);
}
