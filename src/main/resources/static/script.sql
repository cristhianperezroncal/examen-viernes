--CREATE DATABASE IF NOT EXISTS trainingFinalV;
--USE trainingFinalV;

--DROP DATABASE trainingFinalV;

CREATE TABLE IF NOT EXISTS Ingredient (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  ingredientPrice DECIMAL(19,2) NOT NULL
);

CREATE TABLE IF NOT EXISTS DishType (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS Dish (
  id INT AUTO_INCREMENT PRIMARY KEY,
  idDishType INT NOT NULL,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255),
  dishPrice DECIMAL(19,2) NOT NULL,
  FOREIGN KEY (idDishType) REFERENCES DishType(id)
);

CREATE TABLE IF NOT EXISTS IngredientXDish (
  id INT AUTO_INCREMENT PRIMARY KEY,
  idIngredient INT NOT NULL,
  idDish INT NOT NULL,
  quantity INT NOT NULL,
  FOREIGN KEY (idIngredient) REFERENCES Ingredient(id),
  FOREIGN KEY (idDish) REFERENCES Dish(id)
);

DROP TABLE IF NOT EXISTS PurchaseOrder;

CREATE TABLE IF NOT EXISTS PurchaseOrder (
  id INT AUTO_INCREMENT PRIMARY KEY,
  --idDish INT NOT NULL,
  totalPrice DECIMAL(19,2) NOT NULL,
  --FOREIGN KEY (idDish) REFERENCES Dish(id)
);

CREATE TABLE IF NOT EXISTS OrderDetail (
  id INT AUTO_INCREMENT PRIMARY KEY,
  idPurchaseOrder INT NOT NULL,
  idDish INT NOT NULL,
  FOREIGN KEY (idPurchaseOrder) REFERENCES PurchaseOrder(id),
  FOREIGN KEY (idDish) REFERENCES Dish(id)
);

INSERT INTO Ingredient (name, ingredientPrice) VALUES  ('Carne de res', 10.50),  ('Pollo', 8.75),  ('Papa', 2.25);

INSERT INTO DishType (name) VALUES  ('Plato fuerte'),  ('Entrada'),  ('Postre');

INSERT INTO Dish (idDishType, name, description, dishPrice) VALUES
  (1, 'Bistec a la plancha', 'Bistec de res a la plancha con verduras', 25.00),
  (1, 'Pollo asado', 'Muslos de pollo asado con papas fritas', 20.00),
  (2, 'Ensalada César', 'Ensalada de lechuga, pollo, queso y aderezo César', 15.00);

INSERT INTO IngredientXDish (idIngredient, idDish, quantity) VALUES  (2, 1, 3),  (1, 2, 3),  (3, 2, 1);

INSERT INTO PurchaseOrder (totalPrice) VALUES  (25.00),  (35.50),  (15.00);

INSERT INTO OrderDetail (idPurchaseOrder, idDish) VALUES  (1, 1),  (2, 2),  (3, 3);



CREATE PROCEDURE GetAllIngredients()
BEGIN
  SELECT * FROM Ingredient;
END;

CALL GetAllIngredients();


CREATE PROCEDURE GetIngredientById(IN ingredientId INT)
BEGIN
  SELECT * FROM Ingredient WHERE id = ingredientId;
END;

CALL GetIngredientById(1);


CREATE PROCEDURE GetAllDishTypes()
BEGIN
  SELECT * FROM DishType;
END;

CALL GetAllDishTypes();

CREATE PROCEDURE GetDishTypeById(IN dishTypeId INT)
BEGIN
    SELECT * FROM DishType where id = dishTypeId;
END;

CALL GetDishTypeById(2);


CREATE PROCEDURE GetAllDishes()
BEGIN
  SELECT * FROM Dish;
END;

CALL GetAllDishes();

CREATE PROCEDURE GetDishById(IN dishId INT)
BEGIN
  SELECT * FROM Dish WHERE id = dishId;
END;

CALL GetDishById(2);


CREATE PROCEDURE GetAllIngredientsXDishes()
BEGIN
  SELECT * FROM IngredientXDish;
END;

CALL GetAllIngredientsXDishes();

CREATE PROCEDURE GetIngredientsByDishId(IN dishId INT)
BEGIN
  SELECT * FROM IngredientXDish WHERE idDish = dishId;
END

CALL GetIngredientsByDishId(2);




CREATE PROCEDURE GetAllPurchaseOrders()
BEGIN
  SELECT * FROM PurchaseOrder;
END;

CALL GetAllPurchaseOrders();

CREATE PROCEDURE GetPurchaseOrderById(IN purchaseOrderId INT)
BEGIN
    SELECT * FROM PurchaseOrder WHERE id = purchaseOrderId;
END;

CALL GetPurchaseOrderById(1);



CREATE PROCEDURE GetAllOrderDetails()
BEGIN
  SELECT * FROM OrderDetail;
END;

CALL GetAllOrderDetails();

CREATE PROCEDURE GetOrderDetailsByPurchaseOrderId(IN purchaseOrderId INT)
BEGIN
    SELECT * FROM OrderDetail WHERE idPurchaseOrder = purchaseOrderId;
END;

CALL GetOrderDetailsByPurchaseOrderId(1);