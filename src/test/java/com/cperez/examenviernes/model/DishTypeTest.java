package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.DishTypeDTO;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DishTypeTest {
    @Test
    public void testConstructorWithDishTypeDTO() {
        DishTypeDTO dishTypeDTO = new DishTypeDTO();
        dishTypeDTO.setName("Test dish type");

        DishType dishType = new DishType(dishTypeDTO);
        assertEquals(dishTypeDTO.getName(), dishType.getName());
    }
}
