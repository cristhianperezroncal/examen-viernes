package com.cperez.examenviernes.model;

import com.cperez.examenviernes.dto.DishDTO;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class DishTest {

    @Test
    public void testCreateDishFromDishDTO() {
        DishDTO dishDTO = new DishDTO();
        dishDTO.setIdDishType(1);
        dishDTO.setName("Arroz con pollo");
        dishDTO.setDescription("Delicioso plato de arroz con pollo.");
        Dish dish = new Dish(dishDTO);
        assertEquals(dishDTO.getIdDishType(), dish.getDishType().getId());
        assertEquals(dishDTO.getName(), dish.getName());
        assertEquals(dishDTO.getDescription(), dish.getDescription());
    }

    @Test
    public void testGetAndSetDishAttributes() {
        // Se crea una instancia de la clase Dish
        Dish dish = new Dish();

        // Se crea una instancia de la clase DishType y se le asigna un valor al atributo id
        DishType dishType = new DishType();
        dishType.setId(1);

        // Se establecen los valores de los atributos de la instancia de Dish
        dish.setDishType(dishType);
        dish.setName("Arroz con pollo");
        dish.setDescription("Delicioso plato de arroz con pollo.");
        dish.setDishPrice(10.50);

        // Se crean instancias de la clase IngredientXDish y se les asignan valores
        IngredientXDish ingredientXDish1 = new IngredientXDish();
        ingredientXDish1.setIngredient(new Ingredient(1,"Pollo", 5.0));
        ingredientXDish1.setQuantity(1);

        IngredientXDish ingredientXDish2 = new IngredientXDish();
        ingredientXDish2.setIngredient(new Ingredient(2,"Arroz", 2.5));
        ingredientXDish2.setQuantity(2);

        // Se establece la lista de IngredientXDish en el atributo ingredients de la instancia de Dish
        dish.setIngredients(Arrays.asList(ingredientXDish1, ingredientXDish2));

        // Se realizan las comprobaciones con los métodos getter de los atributos de la instancia de Dish
        assertEquals(dishType.getId(), dish.getDishType().getId());
        assertEquals("Arroz con pollo", dish.getName());
        assertEquals("Delicioso plato de arroz con pollo.", dish.getDescription());
        assertEquals(10.50, dish.getDishPrice());
        assertEquals(Arrays.asList(ingredientXDish1, ingredientXDish2), dish.getIngredients());
    }

    @Test
    public void testEquals() {
        Dish dish1 = new Dish();
        Dish dish2 = new Dish();
        assertEquals(dish1, dish2); // Los dos objetos deben ser iguales por defecto.

        dish1.setName("Arroz con pollo");
        assertNotEquals(dish1, dish2); // Los objetos no son iguales si tienen diferentes nombres.

        dish2.setName("Arroz con pollo");
        assertEquals(dish1, dish2); // Ahora los objetos deben ser iguales porque tienen el mismo nombre.
    }

}

