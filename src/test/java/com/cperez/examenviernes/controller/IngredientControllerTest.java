package com.cperez.examenviernes.controller;
import ch.qos.logback.core.net.server.Client;
import com.cperez.examenviernes.dto.IngredientDTO;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.service.IngredientService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(IngredientController.class)
public class IngredientControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private IngredientService ingredientService;



    @Test
    public void getIngredientsTest() throws Exception {
        // Arrange
        Ingredient ingredient1 = new Ingredient(1, "Sal", 2.90);
        Ingredient ingredient2 = new Ingredient(2, "Pimienta", 2.90);
        List<Ingredient> expectedIngredients  = Arrays.asList(ingredient1, ingredient2);

        int expectedStatus = HttpStatus.OK.value();

        when(ingredientService.getIngredients()).thenReturn(expectedIngredients);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/ingredients"))
                .andExpect(status().isOk())
                .andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();
        List<Ingredient> actualIngredients = new ObjectMapper().readValue(content, new TypeReference<List<Ingredient>>() {});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedIngredients.size(), actualIngredients.size());
        assertEquals(expectedIngredients, actualIngredients);

    }

    @Test
    void getIngredientsErrorTest() throws Exception{
        //Devuelve el código de respuesta de la petición
        //int expectedStatus = 500;
        //
        int expectedStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
        when(ingredientService.getIngredients()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/ingredients")).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void getIngredientByIdTest() throws Exception {
        // Arrange
        int id = 1;
        Ingredient expectedIngredient = new Ingredient(id, "Sal", 2.90);

        when(ingredientService.getIngredientByid(id)).thenReturn(expectedIngredient);

        // Act
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/ingredients/" + id))
                .andExpect(status().isOk())
                .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        Ingredient actualIngredient = new ObjectMapper().readValue(content, Ingredient.class);

        // Assert
        assertNotNull(actualIngredient);
        assertEquals(expectedIngredient, actualIngredient);
    }

    @Test
    void getIngredientByIdErrorTest() throws Exception {
        // Arrange
        int id = 1;
        Ingredient expectedIngredient = new Ingredient(id, "Sal", 2.90);

        when(ingredientService.getIngredientByid(id)).thenThrow(new NullPointerException("Error"));

        // Act
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/ingredients/" + id))
                .andExpect(status().isInternalServerError())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), actualStatus);
    }

    @Test
    void createdIngredientTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        IngredientDTO ingredientDTO = new IngredientDTO("Cebolla", 2.90);
        Ingredient expectedIngredient = new Ingredient(ingredientDTO);
        expectedIngredient.setId(1);

        //int expectedStatus = 201;
        int expectedStatus = HttpStatus.CREATED.value();
        String bodyString = mapper.writeValueAsString(ingredientDTO);

        when(ingredientService.createIngredient(any(IngredientDTO.class))).thenReturn(expectedIngredient);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/ingredients")
                .contentType(MediaType.APPLICATION_JSON).content(bodyString)).andExpect(status().isCreated()).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Ingredient actualIngredient = mapper.readValue(content, Ingredient.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
        assertEquals(actualIngredient.getIngredientPrice(), expectedIngredient.getIngredientPrice());

    }

    @Test
    void createdIngredientErrorTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        IngredientDTO ingredientDTO = new IngredientDTO("Cebolla", 2.90);
        int expectedStatus = 500;
        String bodyString = mapper.writeValueAsString(ingredientDTO);
        when(ingredientService.createIngredient(any(IngredientDTO.class))).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/ingredients")
                .contentType(MediaType.APPLICATION_JSON).content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        assertEquals(expectedStatus, actualStatus);

    }


}

