package com.cperez.examenviernes.controller;

import com.cperez.examenviernes.dto.DishTypeDTO;
import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.service.DishService;
import com.cperez.examenviernes.service.DishTypeService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DishTypeController.class)
public class DishTypeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DishTypeService dishTypeService;

    @Test
    void getDishesTypeTest() throws Exception {
        DishType dishType1 = new DishType(1, "Plato Especial 1");
        DishType dishType2 = new DishType(2, "Plato Especial 2");
        List<DishType> expectedDishesType = Arrays.asList(dishType1, dishType2);

        int expectedStatus = HttpStatus.OK.value();

        when(dishTypeService.getDishTypes()).thenReturn(expectedDishesType);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishTypes"))
                .andExpect(status().isOk())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        List<DishType> actualDishesType = new ObjectMapper().readValue(content, new TypeReference<List<DishType>>() {});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedDishesType.size(), actualDishesType.size());
        assertEquals(expectedDishesType, actualDishesType);
    }

    @Test
    void getDishesTypeErrorTest() throws Exception {
        int expectedStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
        when(dishTypeService.getDishTypes()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishTypes"))
                .andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void getDishTypeByIdTest() throws Exception {
        int id = 1;
        DishType expectedDishType = new DishType(id, "Plato Especial 1");

        int expectedStatus = HttpStatus.OK.value();

        when(dishTypeService.getDishTypeByid(id)).thenReturn(expectedDishType);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishTypes/"+id))
                .andExpect(status().isOk())
                .andReturn();

        int actualSatatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        DishType actualDishType = new ObjectMapper().readValue(content, DishType.class);

        assertEquals(expectedStatus, actualSatatus);
        assertNotNull(actualDishType);
        assertEquals(expectedDishType, actualDishType);
    }

    @Test
    void getDishTypeByIdErrorTest() throws Exception {
        int id = 1;
        //DishType expectedDishType = new DishType(id, "Plato Especial 1");

        int expectedStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();

        when(dishTypeService.getDishTypeByid(id)).thenThrow(new NullPointerException("Error"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishTypes/"+id))
                .andExpect(status().isInternalServerError())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void createdDishTypeTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DishTypeDTO dishTypeDTO = new DishTypeDTO("Dish Tipo 1");

        DishType expectedDishType = new DishType(dishTypeDTO);
        expectedDishType.setId(1);

        int expectedStatus = HttpStatus.CREATED.value();

        String bodyString = mapper.writeValueAsString(dishTypeDTO);

        when(dishTypeService.createDishType(any(DishTypeDTO.class))).thenReturn(expectedDishType);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/dishTypes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andExpect(status().isCreated()).andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        DishType actualDishType = mapper.readValue(content, DishType.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedDishType.getName(), actualDishType.getName());
    }

    @Test
    void createdDishTypeErrorTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DishTypeDTO dishTypeDTO = new DishTypeDTO("Dish Tipo 1");

        int expectedStatus = 500;
        String bodyString  = mapper.writeValueAsString(dishTypeDTO);

        when(dishTypeService.createDishType(any(DishTypeDTO.class))).thenThrow(new NullPointerException("Error"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/dishTypes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }


}
