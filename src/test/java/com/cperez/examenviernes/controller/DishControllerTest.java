package com.cperez.examenviernes.controller;

import com.cperez.examenviernes.dto.DishDTO;
import com.cperez.examenviernes.dto.IngredientXDishDTO;
import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.model.IngredientXDish;
import com.cperez.examenviernes.service.DishService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DishController.class)
public class DishControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private DishService dishService;

    @Test
    void getDishesTest() throws Exception {
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish1 = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);
        Dish dish2 = new Dish(2, dishType, "Lomo Saltado de Pollo", "Plato con lomo", 2.90, ingredients);
        List<Dish> expectedDishes = Arrays.asList(dish1, dish2);

        int expectedStatus = HttpStatus.OK.value();

        when(dishService.getDishes()).thenReturn(expectedDishes);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishes"))
                .andExpect(status().isOk())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        List<Dish> actualDishes = new ObjectMapper().readValue(content, new TypeReference<List<Dish>>() {});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedDishes.size(), actualDishes.size());
        assertEquals(expectedDishes, actualDishes);

    }

    @Test
    void getDishesErrorTest() throws Exception {
        int expectedStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
        when(dishService.getDishes()).thenThrow(new NullPointerException("Error"));
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishes"))
                .andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void getDishByIdTest() throws Exception {
        int id = 1;
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish expectedDish = new Dish(id, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        int expectedStatus = HttpStatus.OK.value();

        when(dishService.getDishByid(id)).thenReturn(expectedDish);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishes/"+id))
                .andExpect(status().isOk())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();
        Dish actualDish = new ObjectMapper().readValue(content, Dish.class);

        assertEquals(expectedStatus, actualStatus);
        assertNotNull(actualDish);
        assertEquals(expectedDish, actualDish);


    }

    @Test
    void getDishByIdErrorTest() throws Exception {
        int id = 1;
        //DishType dishType = new DishType(1, "Plato Especial");
        //Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        //IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        //IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        //List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        //Dish expectedDish = new Dish(id, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        int expectedStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();

        when(dishService.getDishByid(id)).thenThrow(new NullPointerException("Error"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/dishes/"+id))
                .andExpect(status().isInternalServerError())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);

    }

    @Test
    void createdDishTest() throws  Exception {
        ObjectMapper mapper = new ObjectMapper();
        IngredientXDishDTO ingredientXDish1 = new IngredientXDishDTO(1, 2);
        IngredientXDishDTO ingredientXDish2 = new IngredientXDishDTO(1, 3);
        ArrayList<IngredientXDishDTO> ingredienstDTO = new ArrayList<>(Arrays.asList(ingredientXDish1, ingredientXDish2));
        DishDTO dishDTO = new DishDTO(1, "Dish 1", "Dish num 1", ingredienstDTO);

        Dish expectedDish = new Dish(dishDTO);
        expectedDish.setId(1);

        int expectedStatus = HttpStatus.CREATED.value();
        String bodyString = mapper.writeValueAsString(dishDTO);

        when(dishService.createDish(any(DishDTO.class))).thenReturn(expectedDish);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/dishes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andExpect(status().isCreated()).andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        Dish actualDish = mapper.readValue(content, Dish.class);

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedDish.getName(), actualDish.getName());
        assertEquals(expectedDish.getDescription(), actualDish.getDescription());
        assertEquals(expectedDish.getDishPrice(), actualDish.getDishPrice());

    }

    @Test
    void createdDishErrorTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        IngredientXDishDTO ingredientXDish1 = new IngredientXDishDTO(1, 2);
        IngredientXDishDTO ingredientXDish2 = new IngredientXDishDTO(1, 3);
        ArrayList<IngredientXDishDTO> ingredienstDTO = new ArrayList<>(Arrays.asList(ingredientXDish1, ingredientXDish2));
        DishDTO dishDTO = new DishDTO(1, "Dish 1", "Dish num 1", ingredienstDTO);

        int expectedStatus = 500;
        String bodyString = mapper.writeValueAsString(dishDTO);

        when(dishService.createDish(any(DishDTO.class))).thenThrow(new NullPointerException("Error"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/dishes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);

    }



}
