package com.cperez.examenviernes.controller;

import com.cperez.examenviernes.dto.DishDTO;
import com.cperez.examenviernes.dto.OrderDetailDTO;
import com.cperez.examenviernes.dto.PurchaseOrderDTO;
import com.cperez.examenviernes.model.*;
import com.cperez.examenviernes.service.DishService;
import com.cperez.examenviernes.service.PurchaseOrderService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.TypeRef;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PurchaseOrderController.class)
public class PurchaseOrderControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PurchaseOrderService purchaseOrderService;

    @Test
    void getPurchaseOrdersTest() throws Exception {
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        OrderDetail orderDetail1 = new OrderDetail(1, dish);
        OrderDetail orderDetail2 = new OrderDetail(2, dish);

        List<OrderDetail> orderDetails = Arrays.asList(orderDetail1, orderDetail2);

        PurchaseOrder purchaseOrder1 = new PurchaseOrder(1, 2.90, orderDetails);
        PurchaseOrder purchaseOrder2 = new PurchaseOrder(2, 2.90, orderDetails);

        List<PurchaseOrder> expectedPurchaseOrders = Arrays.asList(purchaseOrder1, purchaseOrder2);

        int expectedStatus = HttpStatus.OK.value();

        when(purchaseOrderService.getPurchaseOrders()).thenReturn(expectedPurchaseOrders);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/purchaseOrders"))
                .andExpect(status().isOk())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        List<PurchaseOrder> actualPurchaseOrders = new ObjectMapper().readValue(content, new TypeReference<List<PurchaseOrder>>() {});

        assertEquals(expectedStatus, actualStatus);
        assertEquals(expectedPurchaseOrders.size(), actualPurchaseOrders.size());
        assertEquals(expectedPurchaseOrders, actualPurchaseOrders);

    }

    @Test
    void getPurchaseOrdersErrorTest() throws Exception {
        int expectedStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
        when(purchaseOrderService.getPurchaseOrders()).thenThrow(new NullPointerException("Error"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/purchaseOrders"))
                .andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        assertEquals(expectedStatus, actualStatus);

    }

    @Test
    void getPurchaseOrderByIdTest() throws Exception {
        int id = 1;
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        OrderDetail orderDetail1 = new OrderDetail(1, dish);
        OrderDetail orderDetail2 = new OrderDetail(2, dish);

        List<OrderDetail> orderDetails = Arrays.asList(orderDetail1, orderDetail2);

        PurchaseOrder expectedPurchaseOrder = new PurchaseOrder(id, 2.90, orderDetails);

        int expectedStatus = HttpStatus.OK.value();

        when(purchaseOrderService.getPurchaseOrderById(id)).thenReturn(expectedPurchaseOrder);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/purchaseOrders/"+id))
                .andExpect(status().isOk())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        PurchaseOrder actualPurchaseOrder = new ObjectMapper().readValue(content, PurchaseOrder.class);

        assertEquals(expectedStatus, actualStatus);
        assertNotNull(actualPurchaseOrder);
        assertEquals(expectedPurchaseOrder, actualPurchaseOrder);
    }

    @Test
    void getPurchaseOrderByIdErrorTest() throws Exception {
        int id = 1;
        int expectedStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
        when(purchaseOrderService.getPurchaseOrderById(id)).thenThrow(new NullPointerException("Error"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/v1/purchaseOrders/"+id))
                .andExpect(status().isInternalServerError())
                .andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }

    @Test
    void createdPurchaseOrderTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        OrderDetailDTO orderDetailDTO1 = new OrderDetailDTO(1);
        OrderDetailDTO orderDetailDTO2 = new OrderDetailDTO(2);

        ArrayList<OrderDetailDTO> orderDetailDTOS = new ArrayList<>(Arrays.asList(orderDetailDTO1, orderDetailDTO2));

        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(orderDetailDTOS);

        DishType dishType = new DishType(1, "Plato Especial");

        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);

        Dish dish = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        OrderDetail orderDetail1 = new OrderDetail(1, dish);
        OrderDetail orderDetail2 = new OrderDetail(2, dish);

        List<OrderDetail> orderDetails = Arrays.asList(orderDetail1, orderDetail2);


        PurchaseOrder expectedPurchaseOrder = new PurchaseOrder(1, 20.90, orderDetails);

        int expectedStatus = HttpStatus.CREATED.value();
        String bodyString = mapper.writeValueAsString(purchaseOrderDTO);

        when(purchaseOrderService.createPurchaseOrder(any(PurchaseOrderDTO.class))).thenReturn(expectedPurchaseOrder);
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/purchaseOrders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andExpect(status().isCreated()).andReturn();

        int actualStatus = mvcResult.getResponse().getStatus();

        String content = mvcResult.getResponse().getContentAsString();

        PurchaseOrder actualPurchaseOrder = mapper.readValue(content, PurchaseOrder.class);

        assertEquals(expectedStatus, actualStatus);
        assertNotNull(actualPurchaseOrder);
        assertEquals(expectedPurchaseOrder, actualPurchaseOrder);
    }

    @Test
    void createdPurchaseOrderErrorTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        OrderDetailDTO orderDetailDTO1 = new OrderDetailDTO(1);
        OrderDetailDTO orderDetailDTO2 = new OrderDetailDTO(2);

        ArrayList<OrderDetailDTO> orderDetailDTOS = new ArrayList<>(Arrays.asList(orderDetailDTO1, orderDetailDTO2));

        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(orderDetailDTOS);

        int expectedStatus = 500;
        String bodyString = mapper.writeValueAsString(purchaseOrderDTO);

        when(purchaseOrderService.createPurchaseOrder(any(PurchaseOrderDTO.class))).thenThrow(new NullPointerException("Error"));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/v1/purchaseOrders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bodyString)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();

        assertEquals(expectedStatus, actualStatus);
    }


}
