package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.DishDTO;
import com.cperez.examenviernes.dto.IngredientXDishDTO;
import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.model.IngredientXDish;
import com.cperez.examenviernes.repository.DishRepository;
import com.cperez.examenviernes.repository.DishTypeRepository;
import com.cperez.examenviernes.repository.IngredientRepository;
import com.cperez.examenviernes.repository.IngredientXDishRepository;
import com.cperez.examenviernes.service.impl.DishServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishServiceTest {
    @Mock
    private DishRepository dishRepository;
    @Mock
    private DishTypeRepository dishTypeRepository;

    @Mock
    private IngredientRepository ingredientRepository;

    @Mock
    private IngredientXDishRepository ingredientXDishRepository;

    @InjectMocks
    private DishServiceImpl dishService;

    @Test
    void getDishesTypeTest() {
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish1 = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);
        Dish dish2 = new Dish(2, dishType, "Lomo Saltado de Pollo", "Plato con lomo", 2.90, ingredients);
        List<Dish> expectedDishes = Arrays.asList(dish1, dish2);

        when(ingredientRepository.getIngredientByid(ingredient.getId())).thenReturn(ingredient);
        when(ingredientXDishRepository.getIngredientXDishesByDishId(dish1.getId())).thenReturn(ingredients);
        when(dishTypeRepository.getDishTypeByid(dishType.getId())).thenReturn(dishType);
        when(dishRepository.getDishes()).thenReturn(expectedDishes);
        List<Dish> actualDishes = dishService.getDishes();
        assertEquals(expectedDishes.size(), actualDishes.size());
        assertEquals(expectedDishes, actualDishes);
    }

    @Test
    void getDishByIdTest() {
        int id = 1;
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish1 = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        Dish expectedDish = new Dish(id, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);
        when(ingredientRepository.getIngredientByid(ingredient.getId())).thenReturn(ingredient);
        when(ingredientXDishRepository.getIngredientXDishesByDishId(dish1.getId())).thenReturn(ingredients);
        when(dishTypeRepository.getDishTypeByid(dishType.getId())).thenReturn(dishType);
        when(dishRepository.getDishByid(id)).thenReturn(expectedDish);

        Dish actualDish = dishService.getDishByid(id);

        assertNotNull(actualDish);
        assertEquals(expectedDish.getName(), actualDish.getName());
        assertEquals(expectedDish, actualDish);

    }

    @Test
    void createdDishTest() {
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDishDTO ingredientXDish1 = new IngredientXDishDTO(1, 2);
        IngredientXDishDTO ingredientXDish2 = new IngredientXDishDTO(1, 3);
        ArrayList<IngredientXDishDTO> ingredienstDTO = new ArrayList<>(Arrays.asList(ingredientXDish1, ingredientXDish2));
        DishDTO dishDTO = new DishDTO(1, "Dish 1", "Dish num 1", ingredienstDTO);

        Dish expectedDish = new Dish(dishDTO);

        when(ingredientRepository.getIngredientByid(ingredient.getId())).thenReturn(ingredient);
        when(dishRepository.createDish(any(Dish.class))).thenReturn(expectedDish);

        Dish actualDish = dishService.createDish(dishDTO);
        assertNotNull(actualDish);
        assertEquals(expectedDish.getName(), actualDish.getName());
        assertEquals(expectedDish.getDishPrice(), actualDish.getDishPrice());

    }


}
