package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.DishTypeDTO;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.repository.DishTypeRepository;
import com.cperez.examenviernes.service.impl.DishTypeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishTypeServiceTest {
    @Mock
    private DishTypeRepository dishTypeRepository;

    @InjectMocks
    private DishTypeServiceImpl dishTypeService;

    @Test
    void getDishesTypeTest() {
        DishType dishType1 = new DishType(1,"Plato tipo 1");
        DishType dishType2 = new DishType(2,"Plato tipo 2");

        List<DishType> expectedDishesType = Arrays.asList(dishType1, dishType2);

        when(dishTypeRepository.getDishTypes()).thenReturn(expectedDishesType);
        List<DishType> actualDishesType = dishTypeService.getDishTypes();
        assertEquals(expectedDishesType.size(), actualDishesType.size());
        assertEquals(expectedDishesType, actualDishesType);
    }

    @Test
    void getDishesTypeByIdTest() {
        int id = 1;
        DishType expectedDishType = new DishType(id,"Plato tipo 1");

        when(dishTypeRepository.getDishTypeByid(id)).thenReturn(expectedDishType);
        DishType actualDisType = dishTypeService.getDishTypeByid(id);

        assertNotNull(actualDisType);
        assertEquals(expectedDishType.getName(), actualDisType.getName());
    }

    @Test
    void createdDishesTypeTest() {
        DishTypeDTO dishTypeDTO = new DishTypeDTO("Tipo 3");

        DishType expectedDishType = new DishType(dishTypeDTO);

        when(dishTypeRepository.createDishType(any(DishType.class))).thenReturn(expectedDishType);

        DishType actualDishType = dishTypeService.createDishType(dishTypeDTO);

        assertNotNull(actualDishType);
        assertEquals(expectedDishType.getName(), actualDishType.getName());
    }




}
