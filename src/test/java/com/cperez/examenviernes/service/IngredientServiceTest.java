package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.IngredientDTO;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.repository.IngredientRepository;
import com.cperez.examenviernes.service.impl.IngredientServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class IngredientServiceTest {

    @Mock
    private IngredientRepository ingredientRepository;

    @InjectMocks
    private IngredientServiceImpl ingredientService;

    @Test
    void getIngredientsTest() {
        Ingredient ingredient1 = new Ingredient(1,"Cebolla", 2.70);
        Ingredient ingredient2 = new Ingredient(2,"Rocoto", 1.70);
        List<Ingredient> expectedIngredients = new ArrayList<>();
        expectedIngredients.add(ingredient1);
        expectedIngredients.add(ingredient2);
        when(ingredientRepository.getIngredients()).thenReturn(expectedIngredients);
        List<Ingredient> actualIngredients = ingredientService.getIngredients();
        assertEquals(expectedIngredients.size(), actualIngredients.size());
        assertEquals(expectedIngredients, actualIngredients);
    }

    @Test
    void getIngredientByIdTest() {
        int id = 1;
        Ingredient expectedIngredient = new Ingredient(1,"Cebolla", 2.70);
        when(ingredientRepository.getIngredientByid(id)).thenReturn(expectedIngredient);
        Ingredient actualIngredient = ingredientService.getIngredientByid(id);
        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
        assertEquals(actualIngredient.getIngredientPrice(), expectedIngredient.getIngredientPrice());
    }

    @Test
    void createIngredientTest() {
        IngredientDTO ingredientDTO = new IngredientDTO();
        ingredientDTO.setName("Cebolla");
        ingredientDTO.setIngredientPrice(2.70);

        Ingredient expectedIngredient = new Ingredient(ingredientDTO);

        when(ingredientRepository.createIngredient(any(Ingredient.class))).thenReturn(expectedIngredient);

        Ingredient actualIngredient = ingredientService.createIngredient(ingredientDTO);

        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
        assertEquals(actualIngredient.getIngredientPrice(), expectedIngredient.getIngredientPrice());

    }

}
