package com.cperez.examenviernes.service;

import com.cperez.examenviernes.dto.OrderDetailDTO;
import com.cperez.examenviernes.dto.PurchaseOrderDTO;
import com.cperez.examenviernes.model.*;
import com.cperez.examenviernes.repository.DishRepository;
import com.cperez.examenviernes.repository.DishTypeRepository;
import com.cperez.examenviernes.repository.OrderDetailRepository;
import com.cperez.examenviernes.repository.PurchaseOrderRepository;
import com.cperez.examenviernes.service.impl.PurchaseOrderServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PurchaseOrderServiceTest {
    @Mock
    private PurchaseOrderRepository purchaseOrderRepository;
    @Mock
    private OrderDetailRepository orderDetailRepository;
    @Mock
    private DishRepository dishRepository;
    @Mock
    private DishTypeRepository dishTypeRepository;

    @InjectMocks
    private PurchaseOrderServiceImpl purchaseOrderService;


    @Test
    void getPurchaseOrdersTest() {
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        OrderDetail orderDetail1 = new OrderDetail(1, dish);
        OrderDetail orderDetail2 = new OrderDetail(2, dish);

        List<OrderDetail> orderDetails = Arrays.asList(orderDetail1, orderDetail2);

        PurchaseOrder purchaseOrder1 = new PurchaseOrder(1, 2.90, orderDetails);
        PurchaseOrder purchaseOrder2 = new PurchaseOrder(2, 2.90, orderDetails);

        List<PurchaseOrder> expectedPurchaseOrders = Arrays.asList(purchaseOrder1, purchaseOrder2);


        when(purchaseOrderRepository.getPurchaseOrders()).thenReturn(expectedPurchaseOrders);

        List<PurchaseOrder> actualPurchaseOrders = purchaseOrderService.getPurchaseOrders();

        assertEquals(expectedPurchaseOrders.size(), actualPurchaseOrders.size());

    }

    @Test
    void getPurchaseOrderByIdTest() {
        int id = 1;
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);

        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);

        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        OrderDetail orderDetail1 = new OrderDetail(1, dish);
        OrderDetail orderDetail2 = new OrderDetail(2, dish);

        List<OrderDetail> orderDetails = Arrays.asList(orderDetail1, orderDetail2);



        PurchaseOrder expectedPurchaseOrder = new PurchaseOrder(id, 2.90, orderDetails);



        when(dishTypeRepository.getDishTypeByid(dishType.getId())).thenReturn(dishType);
        when(dishRepository.getDishByid(dish.getId())).thenReturn(dish);
        when(orderDetailRepository.getOrderDetailByPurchaseOrderId(expectedPurchaseOrder.getId())).thenReturn(orderDetails);
        when(purchaseOrderRepository.getPurchaseOrderById(id)).thenReturn(expectedPurchaseOrder);

        PurchaseOrder actualPurchaseOrder = purchaseOrderService.getPurchaseOrderById(id);

        assertNotNull(actualPurchaseOrder);
        assertEquals(expectedPurchaseOrder.getTotalPrice(), actualPurchaseOrder.getTotalPrice());
    }


    @Test
    void createdPurchaseOrderTest() {

        OrderDetailDTO orderDetailDTO1 = new OrderDetailDTO(1);
        OrderDetailDTO orderDetailDTO2 = new OrderDetailDTO(2);

        ArrayList<OrderDetailDTO> orderDetailDTOS = new ArrayList<>(Arrays.asList(orderDetailDTO1, orderDetailDTO2));

        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(orderDetailDTOS);

        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);
        Dish dish2 = new Dish(2, dishType, "Lentejas con pescado", "Plato con pescado", 2.90, ingredients);

        OrderDetail orderDetail1 = new OrderDetail(1, dish);
        OrderDetail orderDetail2 = new OrderDetail(2, dish2);

        List<OrderDetail> orderDetails = Arrays.asList(orderDetail1, orderDetail2);

        PurchaseOrder expectedPurchaseOrder = new PurchaseOrder(1, 2.90, orderDetails);

        for (OrderDetail orderDetail : orderDetails) {
            when(dishRepository.getDishByid(orderDetail.getId())).thenReturn(orderDetail.getDish());
        }

        when(purchaseOrderRepository.createPurchaseOrder(any(PurchaseOrder.class))).thenReturn(expectedPurchaseOrder);

        PurchaseOrder actualPurchaseOrder = purchaseOrderService.createPurchaseOrder(purchaseOrderDTO);

        assertNotNull(actualPurchaseOrder);

    }



}
