package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.Dish;
import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.model.IngredientXDish;
import com.cperez.examenviernes.repository.impl.DishRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DishRepositoryTest {
    @Mock
    JdbcTemplate jdbcTemplate;
    @InjectMocks
    private DishRepositoryImpl dishRepository;

    @Test
    void getDishesTest() {
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish dish1 = new Dish(1, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);
        Dish dish2 = new Dish(2, dishType, "Lomo Saltado de Pollo", "Plato con lomo", 2.90, ingredients);
        List<Dish> expectedDishes = Arrays.asList(dish1, dish2);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedDishes);

        List<Dish> actualDishes = dishRepository.getDishes();

        assertEquals(expectedDishes.size(), actualDishes.size());
        assertEquals(expectedDishes, actualDishes);

    }

    @Test
    void getDishByIdTest() {
        int id = 1;
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish expectedDish = new Dish(id, dishType, "Lomo Saltado", "Plato con lomo", 2.90, ingredients);

        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class), eq(id))).thenReturn(expectedDish);

        Dish actualDish = dishRepository.getDishByid(id);

        assertNotNull(actualDish);
        assertEquals(expectedDish, actualDish);
    }

    @Test
    void createdDishTest() {
        DishType dishType = new DishType(1, "Plato Especial");
        Ingredient ingredient = new Ingredient(1, "Cebolla", 2.90);
        IngredientXDish ingredientXDish1 = new IngredientXDish(1,ingredient, 2);
        IngredientXDish ingredientXDish2 = new IngredientXDish(1,ingredient, 3);
        List<IngredientXDish> ingredients = Arrays.asList(ingredientXDish1, ingredientXDish2);
        Dish expectedDish = new Dish();
        expectedDish.setDishType(dishType);
        expectedDish.setName("Lomo Saltado");
        expectedDish.setDescription("Plato con lomo");
        expectedDish.setDishPrice(2.90);
        expectedDish.setIngredients(ingredients);

        when(jdbcTemplate.update(any(), any(GeneratedKeyHolder.class))).thenAnswer(invocation -> {
            GeneratedKeyHolder argumentKeyHolder = invocation.getArgument(1);
            argumentKeyHolder.getKeyList().add(Map.of("id", 1));
            return 1;
        });

        Dish createdDish = dishRepository.createDish(expectedDish);

        assertNotNull(createdDish);
        assertEquals(1, createdDish.getId());
    }




}
