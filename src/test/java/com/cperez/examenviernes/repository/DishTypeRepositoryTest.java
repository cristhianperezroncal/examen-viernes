package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.DishType;
import com.cperez.examenviernes.repository.impl.DishTypeRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;



@ExtendWith(MockitoExtension.class)
public class DishTypeRepositoryTest {
    @Mock
    JdbcTemplate jdbcTemplate;

    @InjectMocks
    private DishTypeRepositoryImpl dishTypeRepository;

    @Test
    void getDishtypesTest() {
        DishType dishType1 = new DishType(1, "Plato 1");
        DishType dishType2 = new DishType(2, "Plato 2");
        List<DishType> expectedDishTypes = Arrays.asList(dishType1, dishType2);
        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedDishTypes);
        List<DishType> actualDishTypes = dishTypeRepository.getDishTypes();
        assertEquals(expectedDishTypes.size(), actualDishTypes.size());
        assertEquals(expectedDishTypes, actualDishTypes);
    }

    @Test
    void getDishTypeByIdTest() {
        int id = 1;
        DishType expectedDishType = new DishType(id, "Plato 1");
        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class), eq(id))).thenReturn(expectedDishType);
        DishType actualDishType = dishTypeRepository.getDishTypeByid(id);
        assertNotNull(actualDishType);
        assertEquals(expectedDishType, actualDishType);
        assertEquals(expectedDishType.getName(), actualDishType.getName());
    }


    @Test
    void createDishTypeTest() throws SQLException{

        // Objeto DishType de prueba
        DishType dishType = new DishType();
        dishType.setName("Test DishType");
        // Objeto KeyHolder para almacenar la clave generada
        //KeyHolder keyHolder = new GeneratedKeyHolder();
        // Configuración del comportamiento del mock de JdbcTemplate
        when(jdbcTemplate.update(any(), any(GeneratedKeyHolder.class))).thenAnswer(invocation -> {
            // Obtiene el objeto GeneratedKeyHolder que se pasó como argumento
            GeneratedKeyHolder argumentKeyHolder = invocation.getArgument(1);
            // Agrega una entrada al mapa de claves
            argumentKeyHolder.getKeyList().add(Map.of("id", 11));

            //Revisar esto
            /*PreparedStatementCreator psc = invocation.getArgument(0);
            Connection connection = mock(Connection.class);
            PreparedStatement ps = psc.createPreparedStatement(connection);
            connection.prepareStatement("", new String[] {"id"});
            PreparedStatement ps1 = connection.prepareStatement("");
            when(connection.prepareStatement(any())).thenReturn(ps1);*/


            // Retorna un valor que indica el número de filas afectadas
            return 1;
        });

        // Llamada al método createDishType
        DishType createdDishType = dishTypeRepository.createDishType(dishType);
        verify(jdbcTemplate).update(any(PreparedStatementCreator.class), any(GeneratedKeyHolder.class));
        assertNotNull(createdDishType);
        assertEquals(11, createdDishType.getId());
        assertEquals(createdDishType.getName(), dishType.getName());
    }

    @Test
    void mapToDishTypeTest() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        DishType expectedDishType = new DishType(1, "Plato 1");
        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("Plato 1");
        DishType actualDishType = dishTypeRepository.mapToDishType(resultSet);
        assertNotNull(actualDishType);
        assertEquals(expectedDishType.getId(), actualDishType.getId());
        assertEquals(expectedDishType.getName(), actualDishType.getName());
    }

}
