package com.cperez.examenviernes.repository;

import com.cperez.examenviernes.model.Ingredient;
import com.cperez.examenviernes.repository.impl.IngredientRepositoryImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IngredientRepositoryTest {
    @Mock
    JdbcTemplate jdbcTemplate;
    @Mock
    private SimpleJdbcInsert simpleJdbcInsert;

    @InjectMocks
    private IngredientRepositoryImpl ingredientRepository;

    @BeforeAll
    void setUp() {
        jdbcTemplate = mock(JdbcTemplate.class);
        simpleJdbcInsert = mock(SimpleJdbcInsert.class);
        given(simpleJdbcInsert.withTableName(anyString())).willReturn(simpleJdbcInsert);
        ingredientRepository = new IngredientRepositoryImpl(jdbcTemplate, simpleJdbcInsert);
    }


    @Test
    void getIngredientsTest() {
        Ingredient ingredient1 = new Ingredient(1,"Cebolla", 2.70);
        Ingredient ingredient2 = new Ingredient(2,"Rocoto", 1.70);
        List<Ingredient> expectedIngredients = new ArrayList<>();
        expectedIngredients.add(ingredient1);
        expectedIngredients.add(ingredient2);
        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(expectedIngredients);
        List<Ingredient> actualIngredients = ingredientRepository.getIngredients();
        assertEquals(expectedIngredients.size(), actualIngredients.size());
        assertEquals(expectedIngredients, actualIngredients);
    }

    @Test
    void getIngredientByIdTest() {
        int id = 1;
        Ingredient expectedIngredient = new Ingredient(1,"Cebolla", 2.70);
        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class), anyInt())).thenReturn(expectedIngredient);
        Ingredient actualIngredient = ingredientRepository.getIngredientByid(id);
        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
        assertEquals(actualIngredient.getIngredientPrice(), expectedIngredient.getIngredientPrice());
    }

    @Test
    void testCreateIngredient() {
        Ingredient expectedIngredient = new Ingredient();
        expectedIngredient.setName("Cebolla");
        expectedIngredient.setIngredientPrice(1.90);
        int generatedId = 2;
        given(simpleJdbcInsert.executeAndReturnKey(any(Map.class))).willReturn(generatedId);
        Ingredient actualIngredient = ingredientRepository.createIngredient(expectedIngredient);
        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getId(), expectedIngredient.getId());
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
    }

    @Test
    void testCreateIngredientWithNullName() {
        Ingredient ingredient = new Ingredient();
        ingredient.setName(null);
        ingredient.setIngredientPrice(1.90);
        assertThrows(NullPointerException.class, () -> {
            ingredientRepository.createIngredient(ingredient);
        });
    }

    @Test
    void mapToIngredient() throws SQLException {
        ResultSet resultSet = mock(ResultSet.class);
        Ingredient expectedIngredient = new Ingredient(1,"Cebolla", 2.70);
        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("name")).thenReturn("Cebolla");
        when(resultSet.getDouble("ingredientPrice")).thenReturn(2.70);
        Ingredient actualIngredient = ingredientRepository.mapToIngredient(resultSet);
        assertNotNull(actualIngredient);
        assertEquals(actualIngredient.getId(), expectedIngredient.getId());
        assertEquals(actualIngredient.getName(), expectedIngredient.getName());
        assertEquals(actualIngredient.getIngredientPrice(), expectedIngredient.getIngredientPrice());
    }
}
